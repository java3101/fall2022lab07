package com.shapes;
/*
 * Thomas Roos
 * 2139198
 */

import com.shapes.code.Cylinder;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CylinderTest {
    
    Cylinder cyl = new Cylinder(1.0, 2.0);

    @Test 
    public void getMethodsTests() {
        assertEquals(1.0, cyl.getHeight(),0);
        assertEquals(2.0, cyl.getRadius(),0);
    } 

    @Test
    public void getVolumeTest() {
        assertEquals(12.56, cyl.getVolume(),0.1);
    }

    @Test
    public void getSurfaceAreaTest() {
        assertEquals(236.87, cyl.getSurfaceArea(),0.1);
    }
}
 