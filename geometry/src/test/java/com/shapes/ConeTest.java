package com.shapes;
import com.shapes.code.Cone;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ConeTest {
    /*Kayci Nicole Davila
     * 2141560
     */

    Cone cone1 = new Cone(1.0,2.0);

    @Test
    public void geMethodsTest(){
        assertEquals(1.0, cone1.getHeight(),0);
        assertEquals(2.0, cone1.getRadius(),0);
    }

    @Test
    public void getVolumeTest(){
        assertEquals(6.28,cone1.getVolume(),0.1);
    }

    @Test
    public void getSurfaceAreaTest(){
        assertEquals(26.61,cone1.getSurfaceArea(),0.1);
    }
}
