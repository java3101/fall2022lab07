package com.shapes.code;

public class Cylinder {
    private double height;
    private double radius;

    public Cylinder(double height, double radius) {
        if (height <= 0) {
            this.height = 1.0;
        } else {
            this.height = height;
        }
        if (radius <= 0) {
            this.radius = 2.0;
        } else {
            this.radius = radius;
        }
    }

    public double getHeight() {
        return this.height;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 2)*this.height;
    }

    public double getSurfaceArea() {
        return 2*Math.PI * (Math.pow(this.radius,2)+2)*Math.PI*this.radius*this.height;

    }
    
}