package com.shapes.code;

public class Cone {
    private double height;
    private double radius;

    public Cone(double height, double radius) {
        if (height <= 0) {
            this.height = 1.0;
        } else {
            this.height = height;
        }
        if (radius <= 0) {
            this.radius = 2.0;
        } else {
            this.radius = radius;
        }
    }

    public double getHeight() {
        return this.height;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getVolume() {
        return Math.PI*Math.pow(this.radius,2)*(this.height/2);
    }

    public double getSurfaceArea() {
        return Math.PI*this.radius*(this.radius + Math.sqrt(Math.pow(this.height,2) + Math.pow(this.radius,2)));
    }
}